import React from 'react';

import './styles.css';

import BreakingBadDetail from './BreakingDetail';
import BreakingBadList from './BreakingList';


class BreakingBad extends React.Component {
    render() {
        return (
            <div>
                <main className="boxmain">
                    <div className="box">
                        <BreakingBadDetail/>
                        <BreakingBadList/>
                    </div>
                </main>
            </div>
        );
    }
}

export default BreakingBad;