import React from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';


import './App.css';

import BreakingBad from './components/BreakingBad';
import routes from './config/routes';

function App() {
  return (
    <Router>
      <div className="App">
        <header>
          <ul className="boxtitle">
            <li className="title">
              <Link to={routes.breakingBad} className="link">breaking bad</Link>
            </li>
          </ul>
        </header>
        <Switch>
          <Route exact path={routes.home}>
            <div>Seleccione una sección en la cabecera de la aplicación</div>
          </Route>
          <Route exact path={routes.breakingList}>
            <breakingList />
          </Route>
          <Route>
            <div>
              <h4>Página no encontrada</h4>
              <Link to={routes.home}>Ir a la home</Link>
            </div>
          </Route>
        </Switch>
        <BreakingBad/>
      </div>
    </Router>
  );
}

export default App;
